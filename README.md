# Terraform templates

## Intro

Terraform templates for deployment to EC2 or EKS.

## Overview

*   [x] **Can be modified and extended**
*   [x] **Tested**

### Requirements

* AWS account
* Terraform v1.1.5+


### Install

Use git to clone this repository locally.
